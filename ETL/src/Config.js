import fs from 'fs'

export default class Config {
  constructor() {
    this.folder = './config'
    this.sources = []
    this.bindFunctions()
  }

  bindFunctions() {
    this.loadFiles = this.loadFiles.bind(this)
    this.loadFile = this.loadFile.bind(this)
    this.parseFile = this.parseFile.bind(this)
  }

  load() {
    const files = fs.readdirSync(this.folder)
    this.loadFiles(files)
    return this.sources
  }

  loadFiles(files) {
    for (const filename of files) {
      this.loadFile(filename)
    }
  }

  loadFile(filename) {
    if(filename == '.gitignore'){
      return
    }
    
    const filePath = this.folder + '/' + filename
    const data = fs.readFileSync(filePath)
    this.parseFile(data)
  }

  parseFile(data) {
    const jsonData = JSON.parse(data)
    this.sources.push(jsonData)
  }
}
