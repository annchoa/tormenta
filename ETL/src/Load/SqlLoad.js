import mysql from 'mysql'
import normalizers from '../Transform/Normalizers/index.js'

export default class SqlLoad {
    constructor(config) {
        this.config = config
    }

    buildQuery(entries) {
        const columns = this.getHeaders()
        const rows = this.getValues(entries)
        const table = this.config.table_name

        const queryString = `INSERT INTO ${table} (${columns}) VALUES ${rows}`

        this.executeQuery(queryString)
    }

    getHeaders() {
        let headers = ''
        for (const field of this.config.fields) {
            if (headers != '') {
                headers += ', '
            }

            headers += field.id
        }
        return headers
    }

    getValues(entries) {
        let values = ""
        for (const entryId in entries) {
            const entry = entries[entryId]
            let value = '('
            for (const field of this.config.fields) {
                if (value != '(') {
                    value += ', '
                }

                let field_value = entry[field.id]

                if (field_value == undefined || (typeof field_value == 'string' && field_value.trim() == '')) {
                    field_value = 'NULL'
                }

                if ('normalizer' in field) {
                    field_value = normalizers[field.normalizer](field_value)
                }

                if (field.type == 'string') {
                    field_value = '"' + field_value + '"'
                }

                value += field_value

            }
            value += '), '
            values += value
        }
        return values.slice(0, -2)
    }

    executeQuery(queryString) {
        let connection = mysql.createConnection({
            host: 'db',
            user: 'root',
            password: 'root',
            database: 'tormenta',
        })

        connection.connect(function (err) {
            if (err) throw err
            connection.query(queryString, (err, rows, fields) => {
                if (err) throw err
                console.log('Query was successfully added')
                connection.end()
                })
            })
    }
}