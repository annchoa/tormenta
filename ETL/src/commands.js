import Extract from "./Extract/Extract.js"
import converters from './Transform/Converters/index.js'
import Config from "./Config.js"
import SqlLoad from "./Load/SqlLoad.js"

const commands = {
  'extract': extractAll,
  'transformAndLoad': transformAndLoad,
}

function extractAll(sources) {
  for (const source of sources) {
    extract(source.url, source.filename)
  }
}

function extract(url, filename) {
  console.log('Exctracting ' + filename)
  const extract = new Extract(url, filename)
  extract.download()
}

function transformAndLoad(sources) {
  for (const source of sources) {
    const entries = transform(source)
    load(source, entries)
  }
}

function transform(config) {
  console.log('Transforming ' + config.filename)
  const converterClass = converters[config.type]
  const converter = new converterClass(config)
  converter.convert()
  return converter.entries
}

function load(config, entries) {
  const sqlLoad = new SqlLoad(config)
  sqlLoad.buildQuery(entries)
}

function loadConfig() {
  const config = new Config()
  return config.load()
}

export default function execute(command) {
  const config = loadConfig()
  console.log('Executing command \'' + command + '\'')
  if (command === undefined) {
    printMessage('You need to provide a command. Valid commands: extract, transformAndLoad')
    printMessage('./index.js [command]')
    return
  }

  command = command.replace('\n', '')

  if (!(command in commands)) {
    printMessage('Invalid command. Valid commands: extract, transformAndLoad')
    return
  }

  commands[command](config)
}
