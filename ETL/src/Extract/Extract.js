import fs from 'fs'
import url from 'url'
import https from 'https'

export default class Extract {

  constructor(link, filename) {
    this.link = link
    this.filename = filename
  }

  download() {
    if (this.link == "") {
      return
    }
    
    let options = {
      host: url.parse(this.link).host,
      path: url.parse(this.link).pathname
    };
    const path = './DownloadFiles/' + this.filename + '.xls'

    https.get(options, function (res) {
      let file = fs.createWriteStream(path)

      res.on('data', function (data) {
        file.write(data)
      })
      .on('end', function () {
        file.end();
        console.log('File downloaded to ' + path)
      });
    });
  }
}