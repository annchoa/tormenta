import data from '../standardData.js'

export default function (field) {
    const firstCleanup = field.replace(/\d+/, '').trim()
    const secondCleanup = correctName(firstCleanup).toUpperCase()
    return secondCleanup
}

function correctName(field) {
    const cleanString = field.toUpperCase()

    for (let lineKey in data.provincias) {

        if (isCorrectName(field, lineKey)) {
            return field
        }

        if (data.provincias[lineKey].includes(cleanString)) {
            return lineKey
        }
    }
    return field
}

function isCorrectName(csvKey, functionKey) {
    return csvKey == functionKey
}