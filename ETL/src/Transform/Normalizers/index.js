import deleteDots from './DeleteDots.js'
import provincias from './Provincias.js'
import comunidades from './Comunidades.js'

export default {
    'DeleteDots': deleteDots,
    'Provincias': provincias,
    'Comunidades': comunidades
}