export default function(field) {
    return field.split('.').join('').split(',').join('')
}