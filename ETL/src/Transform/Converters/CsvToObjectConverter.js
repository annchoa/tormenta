import parse from 'csv-parse/lib/sync.js'
import fs from 'fs'

export default class CsvToObjectConverter {

    constructor(config) {
        this.filePath = "./DownloadFiles/" + config.filename + '.csv'
        this.id = config.id
        this.encoding = config.encoding
        this.delimiter = config.delimiter
        this.fields = config.fields
        this.entries = []
    }

    convert() {
        const data = fs.readFileSync(this.filePath, {
            encoding: this.encoding
        })
        
        const records = parse(data, {
            columns: true,
            skip_empty_lines: true,
            delimiter: this.delimiter
        })

        for (const record of records) {
            this.eachEntry(record)
        }
    }

    eachEntry(record) {
        const entry = {}
        this.fields.forEach(field => {
            entry[field['id']] = record[field['csv_column']]
        });
        this.entries[record[this.id]] = entry
    }
}