import CsvToObjectConverter from './CsvToObjectConverter.js'
import PopulationCsvToObjectConverter from './PopulationCsvToObjectConverter.js'
import XlsToObjectConverter from './XlsToObjectConverter.js'

export default {
    'csv': CsvToObjectConverter,
    'population_csv': PopulationCsvToObjectConverter,
    'xls': XlsToObjectConverter
}