import xlsx from 'node-xlsx'

export default class XlsToObjectConverter {

  constructor(config) {
    this.filePath = "./DownloadFiles/" + config.filename + '.xls'
    console.log(this.filePath)
    this.sheets = config.sheets
    this.extra_fields = config.extra_fields
    this.entries = {}
  }

  convert() {
    const allSheets = xlsx.parse(this.filePath)// parses a file

    for (const sheetConfig of this.sheets) {
      const sheet = allSheets[sheetConfig.id]
      console.log(sheet);
      this.eachEntry(sheetConfig, sheet)
    }
  }

  //loop through all entries in the sheet
  eachEntry(config, sheet) {
    let rowIndex = config.firstRow
    let row = sheet['data'][rowIndex]
    let entryId = row[config.firstColumn]

    while (entryId != undefined) {
      if (!(entryId in this.entries)) {
        this.entries[entryId] = {...this.extra_fields}
      }
  
      for (const field of config.fields) {
        this.entries[entryId][field.id] = row[field.column]
      }

      rowIndex++
      row = sheet['data'][rowIndex]
      if (row == undefined) {
        break
      }
      entryId = row[config.firstColumn]
    }
  }
}