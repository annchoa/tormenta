import parse from 'csv-parse/lib/sync.js'
import fs from 'fs'

export default class PopulationCsvToObjectConverter {

    constructor(config) {
        this.filePath = "./DownloadFiles/" + config.filename + '.csv'
        this.id = config.id
        this.encoding = config.encoding
        this.delimiter = config.delimiter
        this.entries = []
    }

    convert() {
        const data = fs.readFileSync(this.filePath, {
            encoding: this.encoding
        })
        const records = parse(data, {
            columns: true,
            skip_empty_lines: true,
            delimiter: this.delimiter
        })

        for (const record of records) {
            this.eachEntry(record)
        }
    }

    //loop through all entries in the sheet
    eachEntry(record) {
        const provincia = record["Provincias"]
        const amountType = record["Sexo"]
        const year = record["Periodo"]
        const population = record["Total"]

        const id = provincia + year
        if (!(id in this.entries)) {
            this.entries[id] = {
                'Anio': year,
                'Provincia': provincia
            }
        }

        this.entries[id][amountType] = population
    }
}