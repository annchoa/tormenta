## Acerca de
Aquí se encuentra información de los datos de la tabla provincias_CGPJ. A continuación se da una breve descripción de los datos que pueden necesitar mayor aclaración. Finalmente, se citan los artículos del Código Penal a los que hace referencia en los datos de Leciones y malos tratos.

## Listado de datos
- Provincia
- Anio
- Total_delitos: Información de delitos con los que se ha iniciado la denuncia y/o nuevos delitos que se han incluido en el proceso.
- Homicidios
- Aborto
- Lesiones_al_feto
- Lesiones_malos_tratos_art_153_CP: revisar artículo 153
- Lesiones_malos_tratos_art_173_CP: revisar artículo 173
- Lesiones_malos_tratos_art_148_ss_CP: revisar artículo 148 y primer apartado del artículo 147
- Contra_la_libertad
- Contra_libertad_e_indemnidad_sexuales
- Contra_el_honor
- Contra_intimidad_y_derecho_a_la_propia_imagen
- Contra_integridad_moral
- Contra_derechos_deberes_familiares
- Quebrantamientos_penas: inclumplimientos de penas
- Quebrantamientos_medidas: incumplimientos de medidas que pueden ser privativa de libertad, Salida del domicilio, Alejamiento, Prohibición de comunicación, Prohibición volver al lugar del delito, Suspensión de la tenencia de armas, Otras de naturaleza penal.
- Otros_delitos

**Órdenes de protección**
- Total_ordenes: total de ordenes iniciadas, no necesariamente concedidas
- Victima_espaniola_mayor_de_edad: nacionalidad de la víctima con órdenes de protección concedidas
- Victima_espaniola_menor_de_edad
- Victima_extranjera_mayor_de_edad
- Victima_extranjera_menor_de_edad
- Denunciado_hombre_espaniol: Nacionalidad del denunciado con órdenes de protección concedidas.
- Denunciado_hombre_extranjero
- Conyuge: tipo de relación con el denunciado
- Exconyuge
- Relac_afectiva: relación que no incluye ser conyuges
- Exrelacion_afectiva
- Total_Relacion: sumatoria entre las relaciones de conyuge, exconyuge, Relac_afectiva y Exrelacion_afectiva.

**DENUNCIAS**
La procedencia de las denuncia puede ser de víctimas o de familiares cuando son presentadas directamente en los juzgados. 

**Mujeres_victimas_vg** 
Datos de asuntos de violencia contra la mujer. Asuntos es como se lo llama en algunos informes.

**Denuncias_recibidas** 
Total de denuncias iniciadas sin tomar en cuenta que han continuado o no en el proceso.

*Presentada_directamente_victima*
*Presentada_directamente_familiares*
**Atestado_policial_denuncia_victima** 
Un atestado es un documento oficial en el que se exponen los hechos, declaraciones e informes relacionados con un delito.
*Atestado_policial_denuncia_familiar*
*Atestado_policial_intervencion_directa_policial*
Parte_lesiones
Servicios_asistencia_terceros_general
Renuncias_proceso: renuncias al proceso o denuncia presentada
Renuncias_espaniolas: renuncia por nacionalidad de la victima
Renuncias_extranjeras


## Artículos del Codigo Penal
### Artículo 153:
1. El que por cualquier medio o procedimiento causare a otro menoscabo psíquico o una lesión de menor gravedad de las previstas en el apartado 2 del artículo 147, o golpeare o maltratare de obra a otro sin causarle lesión, cuando la ofendida sea o haya sido esposa, o mujer que esté o haya estado ligada a él por una análoga relación de afectividad aun sin convivencia, o persona especialmente vulnerable que conviva con el autor, será castigado con la pena de prisión de seis meses a un año o de trabajos en beneficios de la comunidad de treinta y uno a ochenta días y, en todo caso, privación del derecho a la tenencia y porte de armas de un año y un día a tres años, así como, cuando el juez o tribunal lo estime adecuado al interés del menor o persona con discapacidad necesitada de especial protección, inhabilitación para el ejercicio de la patria potestad, tutela, curatela, guarda o acogimiento hasta cinco años.

2. Si la víctima del delito previsto en el apartado anterior fuere alguna de las personas a que se refiere el artículo 173.2, exceptuadas las personas contempladas en el apartado anterior de este artículo, el autor será castigado con la pena de prisión de tres meses a un año o de trabajos en beneficio de la comunidad de treinta y uno a ochenta días y, en todo caso, privación del derecho a la tenencia y porte de armas de un año y un día a tres años, así como, cuando el Juez o Tribunal lo estime adecuado al interés del menor o persona con discapacidad necesitada de especial protección, inhabilitación para el ejercicio de la patria potestad, tutela, curatela, guarda o acogimiento de seis meses a tres años.

3. Las penas previstas en los apartados 1 y 2 se impondrán en su mitad superior cuando el delito se perpetre en presencia de menores, o utilizando armas, o tenga lugar en el domicilio común o en el domicilio de la víctima, o se realice quebrantando una pena de las contempladas en el artículo 48 de este Código o una medida cautelar o de seguridad de la misma naturaleza.

4. No obstante lo previsto en los apartados anteriores, el Juez o Tribunal, razonándolo en sentencia, en atención a las circunstancias personales del autor y las concurrentes en la realización del hecho, podrá imponer la pena inferior en grado.


### Artículo 173
1. El que infligiera a otra persona un trato degradante, menoscabando gravemente su integridad moral, será castigado con la pena de prisión de seis meses a dos años.

Con la misma pena serán castigados los que, en el ámbito de cualquier relación laboral o funcionarial y prevaliéndose de su relación de superioridad, realicen contra otro de forma reiterada actos hostiles o humillantes que, sin llegar a constituir trato degradante, supongan grave acoso contra la víctima.

Se impondrá también la misma pena al que de forma reiterada lleve a cabo actos hostiles o humillantes que, sin llegar a constituir trato degradante, tengan por objeto impedir el legítimo disfrute de la vivienda.

2. El que habitualmente ejerza violencia física o psíquica sobre quien sea o haya sido su cónyuge o sobre persona que esté o haya estado ligada a él por una análoga relación de afectividad aun sin convivencia, o sobre los descendientes, ascendientes o hermanos por naturaleza, adopción o afinidad, propios o del cónyuge o conviviente, o sobre los menores o personas con discapacidad necesitadas de especial protección que con él convivan o que se hallen sujetos a la potestad, tutela, curatela, acogimiento o guarda de hecho del cónyuge o conviviente, o sobre persona amparada en cualquier otra relación por la que se encuentre integrada en el núcleo de su convivencia familiar, así como sobre las personas que por su especial vulnerabilidad se encuentran sometidas a custodia o guarda en centros públicos o privados, será castigado con la pena de prisión de seis meses a tres años, privación del derecho a la tenencia y porte de armas de tres a cinco años y, en su caso, cuando el juez o tribunal lo estime adecuado al interés del menor o persona con discapacidad necesitada de especial protección, inhabilitación especial para el ejercicio de la patria potestad, tutela, curatela, guarda o acogimiento por tiempo de uno a cinco años, sin perjuicio de las penas que pudieran corresponder a los delitos en que se hubieran concretado los actos de violencia física o psíquica.

Se impondrán las penas en su mitad superior cuando alguno o algunos de los actos de violencia se perpetren en presencia de menores, o utilizando armas, o tengan lugar en el domicilio común o en el domicilio de la víctima, o se realicen quebrantando una pena de las contempladas en el artículo 48 o una medida cautelar o de seguridad o prohibición de la misma naturaleza.

En los supuestos a que se refiere este apartado, podrá además imponerse una medida de libertad vigilada.

3. Para apreciar la habitualidad a que se refiere el apartado anterior, se atenderá al número de actos de violencia que resulten acreditados, así como a la proximidad temporal de los mismos, con independencia de que dicha violencia se haya ejercido sobre la misma o diferentes víctimas de las comprendidas en este artículo, y de que los actos violentos hayan sido o no objeto de enjuiciamiento en procesos anteriores.

4. Quien cause injuria o vejación injusta de carácter leve, cuando el ofendido fuera una de las personas a las que se refiere el apartado 2 del artículo 173, será castigado con la pena de localización permanente de cinco a treinta días, siempre en domicilio diferente y alejado del de la víctima, o trabajos en beneficio de la comunidad de cinco a treinta días, o multa de uno a cuatro meses, esta última únicamente en los supuestos en los que concurran las circunstancias expresadas en el apartado 2 del artículo 84.

Las injurias solamente serán perseguibles mediante denuncia de la persona agraviada o de su representante legal.

### Artículo 148
Las lesiones previstas en el apartado 1 del artículo anterior podrán ser castigadas con la pena de prisión de dos a cinco años, atendiendo al resultado causado o riesgo producido:

1.º Si en la agresión se hubieren utilizado armas, instrumentos, objetos, medios, métodos o formas concretamente peligrosas para la vida o salud, física o psíquica, del lesionado.

2.º Si hubiere mediado ensañamiento o alevosía.

3.º Si la víctima fuere menor de doce años o persona con discapacidad necesitada de especial protección.

4.º Si la víctima fuere o hubiere sido esposa, o mujer que estuviere o hubiere estado ligada al autor por una análoga relación de afectividad, aun sin convivencia.

5.º Si la víctima fuera una persona especialmente vulnerable que conviva con el autor.

*Apartado 1 del Artículo 147: 1. El que, por cualquier medio o procedimiento, causare a otro una lesión que menoscabe su integridad corporal o su salud física o mental, será castigado, como reo del delito de lesiones con la pena de prisión de tres meses a tres años o multa de seis a doce meses, siempre que la lesión requiera objetivamente para su sanidad, además de una primera asistencia facultativa, tratamiento médico o quirúrgico. La simple vigilancia o seguimiento facultativo del curso de la lesión no se considerará tratamiento médico.*