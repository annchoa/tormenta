# Información para crear tablas en BD
- Crear la tabla de provincias_CGPJ

``` CREATE TABLE provincias_CGPJ (
Provincia VARCHAR(100), 
Anio INT, 
Total_delitos INT, 
Homicidios INT, 
Aborto INT,  
Lesiones_al_feto INT,  
Lesiones_malos_tratos_art_153_CP INT,  
Lesiones_malos_tratos_art_173_CP INT,  
Lesiones_malos_tratos_art_148_ss_CP INT,  
Contra_la_libertad INT,  
Contra_libertad_e_indemnidad_sexuales INT, 
Contra_el_honor INT,
Contra_intimidad_y_derecho_a_la_propia_imagen INT, 
Contra_integridad_moral INT,  
Contra_derechos_deberes_familiares INT, 
Quebrantamientos_penas INT,  
Quebrantamientos_medidas INT, 
Otros_delitos INT, 
Total_ordenes INT, 
Victima_espaniola_mayor_de_edad INT, 
Victima_espaniola_menor_de_edad INT, 
Victima_extranjera_mayor_de_edad INT, 
Victima_extranjera_menor_de_edad INT, 
Denunciado_hombre_espaniol INT, 
Denunciado_hombre_extranjero INT, 
Conyuge INT, 
Exconyuge INT, 
Relac_afectiva INT, 
Exrelacion_afectiva INT, 
Total_Relacion INT,
Mujeres_victimas_vg,
Denuncias_recibidas,
Presentada_directamente_victima,
Presentada_directamente_familiares,
Atestado_policial_denuncia_victima,
Atestado_policial_denuncia_familiar,
Atestado_policial_intervencion_directa_policial,
Parte_lesiones,
Servicios_asistencia_terceros_general,
Renuncias_proceso,
Renuncias_espaniolas,
Renuncias_extranjeras,
Denuncias_cada_10mil_habitantes,
Denuncias_cada_10mil_mujeres,
Mujeres_victimas_vg_por_10mil_mujeres,
Ratio_victimas_dispensa_declarar_testigo,
Ratio_victimas_dispensa_declarar_testigo_victimas_VG,
Ratio_ordenes_denuncias,
Ratio_ordenes_Mujeres_victimas_vg,
PRIMARY KEY ( Provincia, Anio )
);```

---

- Crear tabla provincias_poblacion_INE

```CREATE TABLE provincias_poblacion_INE (Provincia VARCHAR(100), Anio INT, Total INT, Mujeres INT, Hombres INT, PRIMARY KEY ( Provincia, Anio ));```

---

- Crear tabla provincias_y_comunidades

```CREATE TABLE provincias_y_comunidades (
Provincia varchar(100) NOT NULL,
Comunidad varchar(100) NOT NULL,
CONSTRAINT provincias_y_comunidades_PK PRIMARY KEY (Provincia)
);```

---

- Eliminar datos de una tabla

`TRUNCATE provincias_CGPJ;`