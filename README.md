# Tormenta :zap:
Es una herramienta de centralización, normalización y consulta de datos abiertos sobre la violencia de género. Su objetivo principal es demostrar las posibilidades que ofrece la centralización de datos abiertos y el establecimiento de relaciones entre las diferentes fuentes de datos. Los datos con los que ha trabajado son los del Consejo General del Poder Judicial de España.

Tormenta ha tratado los datos a través de un proceso ETL, muy conocido en la ciencia de datos. Para **reutilizar** este proyecto puedes empezar por el archivo **makefile** que contiene todos los comandos que se pueden ejecutar en una terminal. Es necesario que se ejecute ´start-tormenta´ si se quiere utilizar la secuencia de comandos para procesar los datos. Para conectarla BD con la herramienta de consulta y visualización de estos datos, se debe ejecutar el comando de ´start-redash´. Redash se ejecuta en el puerto 5000.

También se puede estudiar, distribuir o mejorar, para esto último puedes hacer un pull request. Si necesitas más información de Tormenta puedes escribirme a annacha@inf.upv.es

