start-tormenta:
	docker-compose up

start-redash:
	docker-compose -f docker-compose-redash.yml up

ETL-extract:
	docker-compose exec etl npm run extract

ETL-transform-load:
	docker-compose exec etl npm run transformAndLoad

connect-DB:
	docker-compose exec db mysql -u root -proot tormenta